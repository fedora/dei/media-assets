# Media assets

## What is this place?
The Fedora DEI Team uses this repository to save pictures, videos, and slides from every event that the team organizes.

## Process
First, check if there is already a folder for the event you are interested in adding assets to. If yes, create a subfolder with the event name and year. For example, if the main folder is "Fedora Week of Diversity", the subfolder will be "Fedora Week of Diversity 2024". If there is no folder created for your event, just create a new one. If you need help, please ask in the [DEI team Matrix room](https://matrix.to/#/#dei:fedoraproject.org). 

